/* JS comes here */


var width = 320; // We will scale the photo width to this
var height = 0; // This will be computed based on the input stream

var streaming = false;

var video = document.getElementById('video');
var canvas = document.getElementById('canvas');
var photo = document.getElementById('photo');
var startbutton = document.getElementById('startbutton'); 

function startup() {    

    navigator.mediaDevices.getUserMedia({
            video: true,
            audio: false
        })
        .then(function(stream) {
            video.srcObject = stream;
            var playPromise = video.play();
            if (playPromise !== undefined) {
                playPromise.then(_ => {
                  // Automatic playback started!
                  // Show playing UI.
                  // We can now safely pause video...
                  //video.pause();
                })
                .catch(error => {
                  // Auto-play was prevented
                  // Show paused UI.
                });
              }
            // video.play();
        })
        .catch(function(err) {
            console.log("An error occurred: " + err);
        });

    video.addEventListener('canplay', function(ev) {
        if (!streaming) {
            height = video.videoHeight / (video.videoWidth / width);

            if (isNaN(height)) {
                height = width / (4 / 3);
            }

            video.setAttribute('width', width);
            video.setAttribute('height', height);
            canvas.setAttribute('width', width);
            canvas.setAttribute('height', height);
            streaming = true;
        }
    }, false);

    startbutton.addEventListener('click', function(ev) {
        takepicture();
        jQuery('.camera').hide();
        jQuery('.output').fadeIn();
        ev.preventDefault();
    }, false);

    //clearphoto();
}


function clearphoto() {
    var context = canvas.getContext('2d');
    context.fillStyle = "#AAA";
    context.fillRect(0, 0, canvas.width, canvas.height);

    var data = canvas.toDataURL('image/png');
    photo.setAttribute('src', data);
}

function takepicture() {
    var context = canvas.getContext('2d');
    if (width && height) {
        canvas.width = width;
        canvas.height = height;
        context.drawImage(video, 0, 0, width, height);
        var data = canvas.toDataURL('image/png');
        photo.setAttribute('src', data);
        jQuery('input[name="photo"]').val(data);
        jQuery('#nextBtn').prop('disabled', false);
    } else {
        clearphoto();
    }
}

if (jQuery('.camera').length) {
  window.addEventListener('load', startup, false);
}

function deleteFile(btn)
{
    var id = jQuery(btn).data('id');

    if (confirm('Are you sure you want to delete this file?')) {
        
        jQuery.ajax({
            url : '/upload/'+id+'/destroy',
            type : 'POST',
            data : {
                _token: jQuery('meta[name="csrf-token"]').attr('content'),
                _method: 'DELETE'
            },
            success : function(data) {

                if (data.error) {
                    alert(data.message);
                } 
                
                jQuery(btn).parent().parent().remove();
            },
            error: function (e){
                var res = JSON.parse([e.responseText ]);
                alert( res.message );
            }
        });

    }

}

jQuery(document).ready(function (e) {
    
    jQuery('#ajax-upload').click(function(e) {
        
        var formData = new FormData();
        formData.append('file', $('#file-upload')[0].files[0]);
        formData.append('user_id', $(this).data('id'));
        formData.append('_token', $('meta[name="csrf-token"]').attr('content'));

        var btn = jQuery(this);
        btn.html('Uploading...');

        jQuery.ajax({
            url : '/upload',
            type : 'POST',
            data : formData,
            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType
            success : function(data) {

                if (data.error) {
                    alert(data.message);
                } else {
                    var html = '<tr>';
                    html += '<td><a target="_blank" href="/storage/'+data.file.hash_name+'">'+ data.file.name +'</a></td>';
                    html += '<td>'+ data.file.extension +'</td>';
                    html += '<td>'+ data.file.date +'</td>';
                    html += '<td><button onclick="deleteFile(this)" data-id="'+ data.file.id +'" class="btn btn-danger btn-sm">Delete</button></td>';
                    html += '</tr>';

                    jQuery('#file-list tbody').prepend(html);
                }

                btn.html('Upload');
            },
            error: function (e){
                var res = JSON.parse([e.responseText ]);
                alert( res.message );
                btn.html('Upload');
            }
        });
    });
});
