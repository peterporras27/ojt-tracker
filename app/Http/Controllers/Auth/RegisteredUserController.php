<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use Illuminate\Support\Facades\Storage;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $request->validate([
            'first_name' => ['required','string','max:255'],
            'last_name' => ['required','string','max:255'],
            'middle_name' => ['required','string','max:255'],
            'gender' => ['required','string','max:255'],
            'birthday' => ['required','date','max:255'],
            'city' => ['required','string','max:255'],
            'province' => ['required','string','max:255'],
            'phone' => ['required','string','max:255'],
            'course' => ['required','string','max:255'],
            'year_section' => ['required','string','max:255'],
            'student_number' => ['required','string','max:255'],
            'guardian_name' => ['required','string','max:255'],
            'guardian_phone' => ['required','string','max:255'],
            'office_assigned' => ['required','string','max:255'],
            'office_location' => ['required','string','max:255'],
            'photo' => ['required'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

        $image = $request->photo;  // your base64 encoded
        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = str_random(10).'.'.'png';
        // File::put(storage_path(). '/' . $imageName, base64_decode($image));
        Storage::disk('public')->put($imageName, base64_decode($image));

        $user = User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'middle_name' => $request->middle_name,
            'gender' => $request->gender,
            'birthday' => $request->birthday,
            'city' => $request->city,
            'province' => $request->province,
            'phone' => $request->phone,
            'course' => $request->course,
            'year_section' => $request->year_section,
            'student_number' => $request->student_number,
            'guardian_name' => $request->guardian_name,
            'guardian_phone' => $request->guardian_phone,
            'office_assigned' => $request->office_assigned,
            'office_location' => $request->office_location,
            'required_hours' => $request->required_hours,
            'photo' => $imageName,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        event(new Registered($user));

        Auth::login($user);

        return redirect(RouteServiceProvider::HOME);
    }

    public function base64_to_jpeg($base64_string, $output_file) {

        // open the output file for writing
        $ifp = fopen( $output_file, 'wb' ); 

        // split the string on commas
        // $data[ 0 ] == "data:image/png;base64"
        // $data[ 1 ] == <actual base64 string>
        $data = explode( ',', $base64_string );

        // we could add validation here with ensuring count( $data ) > 1
        fwrite( $ifp, base64_decode( $data[ 1 ] ) );

        // clean up the file resource
        fclose( $ifp ); 

        return $output_file; 
    }
}
