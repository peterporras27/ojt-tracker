<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\File;
use App\Models\Log;
use Auth;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if ( Auth::user()->role == 'student' ) {
            return redirect()->route('home');
        }

        $params = [
            'name' => '',
            'year_section' => '',
            'student_number' => '',
            'office_assigned' => '',
            'office_location' => ''
        ];

        $args = [];
        $hasFilter = false;
        $search = false;

        $students = User::where('role','=','student');

        foreach ($params as $param => $val) {
            if ($request->input($param)) {

                if ($param == 'name') {

                    $names = explode(' ', $request->input('name'));

                    if (isset($names[0])) {
                        $students = $students->where('first_name','LIKE','%'.$names[0].'%');    
                    }

                    if (isset($names[2])) {

                        if (isset($names[1])) {
                            $students = $students->where('middle_name','LIKE','%'.$names[1].'%');
                        }

                        $students = $students->where('last_name','LIKE','%'.$names[2].'%');    

                    } else {

                        if (isset($names[1])) {
                            $students = $students->where('last_name','LIKE','%'.$names[1].'%');    
                        }
                    }
                
                } else {

                    $students = $students->where($param,'LIKE','%'.$request->input($param).'%');
                }

                $params[$param] = $request->input($param);
                $hasFilter = true;
                $search = true;
            }
        }

        $students = $students->paginate(10);
        
        return view('students.index',compact('students','params','search'));   
    }

    public function home(Request $request)
    {
        $student = Auth::user();
        $logs = Log::where('user_id','=',$student->id);

        if ( $request->input('from') && $request->input('to') ) {
            // Search logs between specified dates.
            $logs = $logs->whereBetween('created_at', [$request->input('from'), $request->input('to')]);
            $title = 'From: '.date('F j, Y',strtotime($request->input('from')));
            $title .= ' To: '.date('F j, Y',strtotime($request->input('to')));
        } 

        $logs = $logs->get();

        $days = [];
        $time = [
            'hours' => 0,
            'minutes' => 0
        ];

        $def = $request->input('tab') ? false : true;

        $tabs = [
            'home' => $request->input('tab') == 'home' ? true : $def,
            'profile' => $request->input('tab') == 'profile' ? true : false,
            'portfolio' => $request->input('tab') == 'portfolio' ? true : false,
        ];

        if ($logs) 
        {
            foreach($logs as $log) 
            {
                $days[$log->created_at->format('m-d-Y')][$log->created_at->format('A')][$log->type] = $log;
            }
        }

        //return response()->json($days);

        $files = File::where('user_id','=',Auth::user()->id)->get();

        return view('students.show',compact('student','days','time','tabs','files'));
    }


    public function admin_index()
    {
        $admins = User::where('role','=','admin')->paginate(10);
        return view('admins.index',compact('admins'));
    }

    public function admin_create()
    {
        return view('admins.create');
    }

    public function admin_store(Request $request)
    {
        request()->validate([
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'middle_name' => 'required|string',
            'phone' => 'required|string',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|confirmed',
        ]);

        $admin = new User;
        $admin->fill($request->all());
        $admin->password = Hash::make($request->input('password'));
        $admin->role = 'admin';
        $admin->save();

        return redirect()->route('admins')->with('success','Admin successfully registered!');
    }

    public function admin_show($id)
    {

    }
    
    public function admin_edit($id) 
    {
        $user = User::find($id);
        return view('admins.edit',compact('user'));
    }

    public function admin_update(Request $request, $id)
    {
        $admin = User::find($id);
        $data = $request->all();

        $validate = [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'middle_name' => 'required|string',
            'phone' => 'required|string'
        ];

        if (!empty($data['password'])) {

            $validate['password'] = 'required|string|confirmed';
            $admin->password = Hash::make($data['password']);

        } else {

            unset($data['password']);
        }

        if ($data['email'] != $admin->email) {
            
            $validate['email'] = 'required|string|email|max:255|unique:users';
            $admin->email = $data['email'];

        } else {

            unset($data['email']);
        }

        request()->validate($validate);

        $admin->first_name = $data['first_name'];
        $admin->last_name = $data['last_name'];
        $admin->middle_name = $data['middle_name'];
        $admin->phone = $data['phone'];
        $admin->save();

        return redirect()->route('admin.edit',$admin->id)->with('success','Admin details successfully updated.');
    }

    public function admin_destroy(Request $request, $id)
    {
        $admin = User::find($id);

        if ($admin->id == Auth::user()->id) {
            return redirect()->route('admins')->with('error','You are not allowed to delete your own account.');
        }

        if (!$admin) {
            return redirect()->route('admins')->with('error','Admin does not exist.');
        }

        $admin->delete();

        return redirect()->route('admins')->with('success','Admin successfully removed!');
    }
}
