<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\File;
use App\Models\Log;
use Auth;

class StudentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = User::where('role','=','student')->paginate(10);

        return view('students.index',compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('students.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'middle_name' => 'required|string',
            'gender' => 'required|string',
            'birthday' => 'required|date',
            'city' => 'required|string',
            'province' => 'required|string',
            'phone' => 'required|string',
            'course' => 'required|string',
            'year_section' => 'required|string',
            'student_number' => 'required|string',
            'guardian_name' => 'required|string',
            'guardian_phone' => 'required|string',
        ]);

        $data = $request->all();
        

        $student = new User;
        $student->fill($data);
        $student->password = Hash::make($data['password']);
        $student->save();

        return redirect()->route('dashboard')->with('success','Student successfully registered!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $student = User::find($id);

        if (!$student) {
            return redirect()->route('dashboard')->with('error','Student does not exist.');
        }

        if ( $request->input('from') && $request->input('to') ) {
            // Search logs between specified dates.
            $logs = Log::whereBetween('created_at', [$request->input('from'), $request->input('to')])->get();
            $title = 'From: '.date('F j, Y',strtotime($request->input('from')));
            $title .= ' To: '.date('F j, Y',strtotime($request->input('to')));

        } else {

            $logs = Log::where('user_id','=',$id)->get();
        }

        $days = [];
        $time = [
            'hours' => 0,
            'minutes' => 0
        ];

        $def = $request->input('tab') ? false : true;

        $tabs = [
            'home' => $request->input('tab') == 'home' ? true : $def,
            'profile' => $request->input('tab') == 'profile' ? true : false,
            'portfolio' => $request->input('tab') == 'portfolio' ? true : false,
        ];

        if ($request->input('from') && $request->input('to')) {
            $profile = true;
        }

        if ($logs) {
            foreach($logs as $log) {
                $days[$log->created_at->format('m-d-Y')][$log->created_at->format('A')][$log->type] = $log;
            }
        }

        $files = File::where('user_id','=',$student->id)->get();

        return view('students.show',compact('student','days','time','tabs','files'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student = User::find($id);

        if (!$student) {
            return redirect()->route('dashboard')->with('error','Student does not exist.');
        }

        return view('students.edit',compact('student'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $student = User::find($id);

        if (!$student) {
            return redirect()->route('dashboard')->with('error','Student does not exist.');
        }

        $tovalidate = [
            'first_name' => ['required','string','max:255'],
            'last_name' => ['required','string','max:255'],
            'middle_name' => ['required','string','max:255'],
            'gender' => ['required','string','max:255'],
            'birthday' => ['required','date','max:255'],
            'city' => ['required','string','max:255'],
            'province' => ['required','string','max:255'],
            'phone' => ['required','string','max:255'],
            'course' => ['required','string','max:255'],
            'year_section' => ['required','string','max:255'],
            'student_number' => ['required','string','max:255'],
            'guardian_name' => ['required','string','max:255'],
            'guardian_phone' => ['required','string','max:255'],
            'office_assigned' => ['required','string','max:255'],
            'office_location' => ['required','string','max:255']
        ];

        $datas = $request->all();

        // do not allow other users to edit other accounts
        if (Auth::user()->role == 'student') {
            if (Auth::user()->id != $id) {
                return redirect('home')->with('error','Permission not allowed.');
            }
        }

        if ( !empty($request->input('password')) ) {
            $tovalidate['password'] = ['required', 'confirmed'];
            $datas['password'] = Hash::make($datas['password']);
        } else {
            unset($datas['password']);
        }

        if ( $request->input('email') != $student->email ) {
            $tovalidate['email'] = ['required', 'string', 'email', 'max:255', 'unique:users'];
        }

        request()->validate($tovalidate);

        if ( !empty($request->photo) ) {

            $image = $request->photo;  // your base64 encoded
            $image = str_replace('data:image/png;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = str_random(10).'.'.'png';
            Storage::disk('public')->put($imageName, base64_decode($image));

            if ($student->photo) {
                Storage::disk('public')->delete($student->photo);
            }

            $datas['photo'] = $imageName;

        } else {

            unset($datas['photo']);
        }

        $student->fill($datas);
        $student->save();

        return redirect()->route('student.edit',$id)->with('success','Student details successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student = User::find($id);

        if ($student->id == Auth::user()->id) {
            return redirect()->route('dashboard')->with('error','Please try again.');
        }

        if (!$student) {
            return redirect()->route('dashboard')->with('error','Student does not exist.');
        }

        $student->delete();

        return redirect()->route('dashboard')->with('success','Student successfully removed!');
    }

    public function print(Request $request, $id)
    {
        $student = User::find($id);

        if (!$student) {
            return redirect()->route('dashboard')->with('error','Student does not exist.');
        }

        $title = '';
        // Check if filter is available.
        if ( $request->input('from') && $request->input('to') ) {
            // Search logs between specified dates.
            $logs = Log::whereBetween('created_at', [$request->input('from'), $request->input('to')])->get();
            $title = 'From: '.date('F j, Y',strtotime($request->input('from')));
            $title .= ' To: '.date('F j, Y',strtotime($request->input('to')));

        } else {

            $logs = Log::where('user_id','=',$id)->get();
        }

        $days = [];
        $time = [
            'hours' => 0,
            'minutes' => 0
        ];

        $profile = false;

        if ($request->input('from') && $request->input('to')) {
            $profile = true;
        }

        if ($logs) {
            foreach($logs as $log) {
                $days[$log->created_at->format('m-d-Y')][$log->created_at->format('A')][$log->type] = $log->created_at->format('h:i');
            }
        }

        return view('print',compact('student','days','time','profile','title'));
    }

    public function update_log(Request $request, $id) 
    {
        $data = $request->all();
        $d = explode('-', $data['date']);

        $hour = (isset($data['hour'])) ? $data['hour']: '00';

        if ($data['am_pm'] == 'PM') {
            
            $hour = (int) $data['hour']+12;
        }

        $date = $d[2].'-'.$d[0].'-'.$d[1].' '.$hour.':'.$data['minute'].':00';
        
        $log = new Log();
        $log->user_id = $id;
        $log->created_at = $date;
        $log->status = 'pending';
        $log->type = 'time_out';
        $log->save();

        return redirect('home?tab=profile')->with('success','Timeout request successfully sent.');
    }

    public function approve_log($id) 
    {
        $log = Log::find($id);

        if (!$log) {
            return redirect('home?tab=profile')->with('error','Log no longer exist.');
        }

        $log->status = 'approved';
        $log->save();

        return redirect('student/'.$log->user_id.'?tab=profile')->with('success','Timeout request successfully sent.');
    }
}
