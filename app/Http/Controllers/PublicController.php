<?php

namespace App\Http\Controllers;

use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use App\Models\Indigency;
use App\Models\User;
use App\Models\Log;

class PublicController extends Controller
{
    //
    public function index()
    {
        if(auth()->check()) {
            if (auth()->user()->role == 'admin') {
                return redirect('dashboard');
            } else {
                return redirect('home');
            }
        }

        return view('homepage');
    }

    public function qrcode($id)
    {
        $user = User::find($id);
        $img = QrCode::format('png')->size(600)->generate($user->student_number);
        return response($img)->header('Content-type','image/png');
    }

    public function downloadQR($id)
    {
        $user = User::find($id);
        $filename = str_slug($user->first_name.' '.$user->last_name, '-').'.png';
        $tempImage = tempnam(sys_get_temp_dir(), $filename);
        copy(route('qrcode',$user->id), $tempImage);

        return response()->download($tempImage, $filename);
    }

    public function scanner()
    {
        return view('scanner');
    }

    public function log($code)
    {
        $user = User::where('student_number','=',$code)->first();

        $response = [
            'error' => true,
            'message' => 'Please try again.',
            'error_type' => 'danger'
        ];

        if ( $user ) {

            $from_am = date('Y-m-d H:i:s',strtotime(date('Y-m-d').' 00:00:00'));
            $to_am = date('Y-m-d H:i:s',strtotime(date('Y-m-d').' 11:59:00'));

            $from_pm = date('Y-m-d H:i:s',strtotime(date('Y-m-d').' 12:00:00'));
            $to_pm = date('Y-m-d H:i:s',strtotime(date('Y-m-d').' 24:00:00'));

            $time_am = Log::whereBetween('created_at', [$from_am, $to_am])
                ->where('user_id','=',$user->id)
                ->get();

            $time_pm = Log::whereBetween('created_at', [$from_pm, $to_pm])
                ->where('user_id','=',$user->id)
                ->get();

            $logged_out = false;
            $verbage = 'logged in.';

            $log = new Log();
            $log->user_id = $user->id;

            $twominutesago = Log::where('user_id','=',$user->id)
                ->whereBetween('created_at', [now()->subMinutes(2), now()])
                ->first();

            $mornafter = '';
            // check current time
            if (date('A') == 'AM') {

                $logged_out = ( $time_am->count() >= 2 ); 
                $log->type = ( $time_am->count() == 0 ) ? 'time_in':'time_out';
                $mornafter = 'morning';
            }

            if (date('A') == 'PM') {

                $logged_out = ( $time_pm->count() >= 2 );
                $log->type = ( $time_pm->count() == 0 ) ? 'time_in':'time_out';
                $mornafter = 'afternoon';
            }

            if ( $logged_out ) {

                $response['message'] = 'You are already logged out this '.$mornafter.'.';
                $response['error_type'] = 'warning';

                return response()->json($response);
            }

            if ( $twominutesago ) {

                $response['message'] = 'You already logged '.str_replace('time_', '', $twominutesago->type).' two minutes ago.';
                $response['error_type'] = 'warning';

                return response()->json($response);
            }

            $response['error'] = false;
            $response['message'] = 'You have successfully logged '.str_replace('time_', '', $log->type);
            $response['error_type'] = 'success';

            $log->save();
        }

        return response()->json($response);
    }
}
