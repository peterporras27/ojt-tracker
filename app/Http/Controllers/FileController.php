<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Models\File;
use Auth;

class FileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'file'  => 'required|mimes:png,jpeg,jpg,gif,doc,docx,pdf|max:25048',
        ]);

        if ( !empty($request->file) ) {

            $realname = $request->file->getClientOriginalName();
            $extension = $request->file->getClientOriginalExtension();
            $hashName = $request->file->hashName();
            
            $path = $request->file('file')->store('public');

            //store your file into database
            $document = new File();
            $document->name = $realname;
            $document->hash_name = $hashName;
            $document->extension = $extension;
            $document->user_id = $request->input('user_id') ? $request->input('user_id') : Auth::user()->id;
            $document->save();

            $document->date = $document->created_at->format('F j, Y, g:i a');
            
            return response()->json([
                'error' => false,
                'message' => 'File upload successful.',
                'file' => $document
            ]);
        }

        return response()->json([
            'error' => true,
            'message' => 'Error uploading file, please try again.',
            'file' => ''
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $file = File::find($id);
        
        if ($file->hash_name) {
            Storage::disk('public')->delete($file->hash_name);
        }

        $file->delete();

        return response()->json([
            'error' => true,
            'message' => 'File successfully deleted.',
            'file' => ''
        ]);
    }
}
