<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\FileController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'index'])->name('home');
Route::get('/scanner', [PublicController::class, 'scanner'])->name('scanner');
Route::get('/home', [AdminController::class, 'home'])->name('homepage');
Route::get('/dashboard', [AdminController::class, 'index'])->name('dashboard');
Route::get('/log/{code}', [PublicController::class, 'log'])->name('log');

Route::get('/students', [StudentController::class, 'index'])->name('students');
Route::get('/student/create', [StudentController::class, 'create'])->name('student.create');
Route::post('/student/store', [StudentController::class, 'store'])->name('student.store');
Route::get('/student/{id}', [StudentController::class, 'show'])->name('student.show');
Route::get('/student/{id}/edit', [StudentController::class, 'edit'])->name('student.edit');
Route::put('/student/{id}/update', [StudentController::class, 'update'])->name('student.update');
Route::delete('/student/{id}/destroy', [StudentController::class, 'destroy'])->name('student.destroy');
Route::get('/student/{id}/print', [StudentController::class, 'print'])->name('student.print');
Route::post('/student/{id}/log', [StudentController::class, 'update_log'])->name('student.log');
Route::get('/approve/{id}/log', [StudentController::class, 'approve_log'])->name('approve.log');

Route::get('/qr/{id}/code.png', [PublicController::class, 'qrcode'])->name('qrcode');
Route::get('/qr/{id}/download', [PublicController::class, 'downloadQR'])->name('download');

Route::get('/admins', [AdminController::class, 'admin_index'])->name('admins');
Route::get('/admin/create', [AdminController::class, 'admin_create'])->name('admin.create');
Route::post('/admin/store', [AdminController::class, 'admin_store'])->name('admin.store');
Route::get('/admin/{id}', [AdminController::class, 'admin_show'])->name('admin.show');
Route::get('/admin/{id}/edit', [AdminController::class, 'admin_edit'])->name('admin.edit');
Route::put('/admin/{id}/update', [AdminController::class, 'admin_update'])->name('admin.update');
Route::delete('/admin/{id}/destroy', [AdminController::class, 'admin_destroy'])->name('admin.destroy');

Route::get('/file', [FileController::class, 'index'])->name('file');
Route::post('/upload', [FileController::class, 'store'])->name('file.store');
Route::delete('/upload/{id}/destroy', [FileController::class, 'destroy'])->name('file.destroy');

require __DIR__.'/auth.php';
