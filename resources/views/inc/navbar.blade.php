<nav class="navbar navbar-expand-lg navbar-dark bg-dark" aria-label="Main navigation">
	<div class="container">
		<a class="navbar-brand" href="/">OJT Tracker</a>
		<button class="navbar-toggler p-0 border-0" type="button" id="navbarSideCollapse" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
			<ul class="navbar-nav me-auto mb-2 mb-lg-0">
				@if (Route::has('login'))
                    @auth
	                    @if(Auth::user()->role=='admin')
	                    <li class="nav-item">
	                        <a href="{{ url('/') }}" class="nav-link">Students</a>
	                    </li>
	                    <li class="nav-item">
	                        <a href="{{ route('admins') }}" class="nav-link">Admins</a>
	                    </li>
	                    @endif
                    @endauth
	            @endif
			</ul>
			<div class="d-flex">
				<ul class="navbar-nav">
					@if (Route::has('login'))
						@auth
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
									{{Auth::user()->first_name.' '.Auth::user()->last_name}}
								</a>
								<ul class="dropdown-menu" aria-labelledby="navbarDarkDropdownMenuLink">
									<li>
										@if(Auth::user()->role=='admin')
										<a class="dropdown-item" href="{{ route('admin.edit',Auth::user()->id) }}">
											Settings
										</a>
										@else
										<a class="dropdown-item" href="{{ route('student.edit',Auth::user()->id) }}">
											Settings
										</a>
										@endif
									</li>
									<li>
										<form method="POST" action="{{ route('logout') }}">
											@csrf
											<a class="dropdown-item" onclick="event.preventDefault();
                                                this.closest('form').submit();" href="#">Logout</a>
                                        </form>
                                    </li>
								</ul>
							</li>
						@else
							<li class="nav-item">
								<a href="{{ route('login') }}" class="nav-link">Log in</a>
							</li>
	                        @if (Route::has('register'))
	                        <li class="nav-item">
	                            <a href="{{ route('register') }}" class="nav-link">Register</a>
	                        </li>
	                        @endif
                        @endauth
                    @endif
				</ul>
			</div>
		</div>
	</div>
</nav>