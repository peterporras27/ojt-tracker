@include('inc.header')

<div class="container mt-5">
	<div class="row justify-content-center">
		<div class="col-sm-5">

			@include('inc.errors')

			<nav class="mt-3">
				<div class="nav nav-tabs" id="nav-tab" role="tablist">
					<button class="nav-link active" id="nav-scanner-tab" data-bs-toggle="tab" data-bs-target="#nav-scanner" type="button" role="tab" aria-controls="nav-scanner" aria-selected="false">QR Scanner</button>
					<button class="nav-link" id="nav-home-tab" data-bs-toggle="tab" data-bs-target="#nav-home" type="button" role="tab" aria-controls="nav-home" aria-selected="true">Login</button>
					<button class="nav-link" id="nav-profile-tab" data-bs-toggle="tab" data-bs-target="#nav-profile" type="button" role="tab" aria-controls="nav-profile" aria-selected="false">Register</button>
				</div>
			</nav>

			<div class="tab-content bg-white" id="nav-tabContent">
				<div class="tab-pane fade show active" id="nav-scanner" role="tabpanel" aria-labelledby="nav-scanner-tab">
					<div class="tab-pane-wrapper" align="center">
						<div id="reader"></div>
						<div id="loader"></div>
						<button class="btn btn-success w-100 btnscanner" onclick="openScanner()">Open Scanner</button>
						<div class="position-fixed top-0 end-0 p-3" style="z-index: 11">
							<div id="liveToast" class="toast" role="alert" aria-live="assertive" aria-atomic="true">
								<div class="toast-header">
									<strong class="me-auto">QR Scanner</strong>
									<small>Just now</small>
									<button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
								</div>
								<div class="toast-body"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane fade" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
					<div class="tab-pane-wrapper">
						<form method="POST" action="{{ route('login') }}">
							@csrf
							<div class="form-floating">
								<input type="email" name="email" class="form-control" id="floatingInput" placeholder="name@example.com">
								<label for="floatingInput">Email address</label>
							</div>
							<div class="form-floating mt-3">
								<input type="password" name="password" class="form-control" id="floatingPassword" placeholder="Password">
								<label for="floatingPassword">Password</label>
							</div>
							<div class="row mt-3">
								<div class="col-sm-6">
									<div class="checkbox mb-3">
										<label>
											<input type="checkbox" name="remember" value="remember-me"> Remember me
										</label>
									</div>
								</div>
								<div class="col-sm-6 text-end">
									@if (Route::has('password.request'))
					                    <a class="link" href="{{ route('password.request') }}">
					                        {{ __('Forgot your password?') }}
					                    </a>
					                @endif
								</div>
							</div>
							
							<button class="w-100 btn btn-lg btn-primary" type="submit">Sign in</button>
						</form>
					</div>
				</div>
				<div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
					<div class="tab-pane-wrapper">
						<form id="regForm" class="needs-validation" enctype="multipart/form-data" method="POST" action="{{ route('register') }}">
				            @csrf
				            <h3>Register</h3>
				            <div class="tab">
			            		<div class="mt-3 mb-3 form-floating">
									<input type="text" name="first_name" value="{{ old('first_name') }}" class="form-control" id="first_name" placeholder="First Name" oninput="this.className='form-control'" required>
									<label for="first_name">First Name</label>
								</div>
					            <div class="row">
					            	<div class="col-sm-6">
					            		<div class="mb-3 form-floating">
											<input type="text" name="last_name" value="{{ old('last_name') }}" class="form-control" id="last_name" placeholder="Last Name" oninput="this.className='form-control'" required>
											<label for="last_name">Last Name</label>
										</div>
					            	</div>
					            	<div class="col-sm-6">
					            		<div class="mb-3 form-floating">
											<input type="text" name="middle_name" value="{{ old('middle_name') }}" class="form-control" id="middle_name" placeholder="Middle Name" oninput="this.className='form-control'" required>
											<label for="middle_name">Middle Name</label>
										</div>
					            	</div>
					            </div>
					            <div class="mb-3 form-floating">
									<input type="text" name="email" value="{{ old('email') }}" class="form-control" id="regemail" placeholder="Email" oninput="this.className='form-control'" required>
									<label for="regemail">Email</label>
								</div>

								<div class="row">
									<div class="col-sm-6">
										<div class="mb-3 form-floating">
											<input type="password" name="password" class="form-control" id="regpassword" placeholder="*****" autocomplete="new-password" oninput="this.className='form-control'" required>
											<label for="regpassword">Password</label>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="mb-3 form-floating">
											<input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Confirm Password" oninput="this.className='form-control'" required>
											<label for="password_confirmation">Confirm Password</label>
										</div>
									</div>
								</div>
							</div>

							<div class="tab">
			            		<div class="row mt-3">
									<div class="col-sm-4">
										<div class="mb-3 form-floating">
											<select name="gender" name="gender" class="form-control" id="gender">
												<option value="Male"{{ old('gender') == 'Male' ? ' selected':'' }}>Male</option>
												<option value="Female"{{ old('gender') == 'Female' ? ' selected':'' }}>Female</option>
											</select>
											<label for="gender">Gender</label>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="mb-3 form-floating">
											<input type="date" name="birthday" value="{{ old('birthday') }}" class="form-control" id="birthday" placeholder="Birthday" oninput="this.className='form-control'" required>
											<label for="birthday">Birthday</label>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="mb-3 form-floating">
											<input type="text" name="phone" value="{{ old('phone') }}" class="form-control" id="phone" placeholder="Phone Number" oninput="this.className='form-control'" required>
											<label for="phone">Phone Number</label>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="mb-3 form-floating">
											<select name="year_section" class="form-control" id="year_section" oninput="this.className='form-control'" required>
												<option value="">Select</option>
												<option value="4-A"{{ old('year_section') == '4-A' ? ' selected':''}}>4-A</option>
												<option value="4-B"{{ old('year_section') == '4-B' ? ' selected':''}}>4-B</option>
												<option value="4-C"{{ old('year_section') == '4-C' ? ' selected':''}}>4-C</option>
												<option value="4-D"{{ old('year_section') == '4-D' ? ' selected':''}}>4-D</option>
												<option value="4-E"{{ old('year_section') == '4-E' ? ' selected':''}}>4-E</option>
											</select>
											<label for="year_section">Year and Section</label>
										</div>
									</div>
									<div class="col-md-6">
										<div class="mb-3 form-floating">
											<input type="text" name="student_number" value="{{ old('student_number') }}" class="form-control" id="student_number" placeholder="Student Number" oninput="this.className='form-control'" required>
											<label for="student_number">Student Number</label>
										</div>
									</div>
								</div>

								<div class="mb-3 form-floating">
									<select  name="course" value="{{ old('course') }}" class="form-control" id="course" oninput="this.className='form-control'" required>
										<option value="">Select</option>
										<option value="BS Computer Science"{{ old('course') == 'BS Computer Science' ? ' selected':'' }}>BS Computer Science</option>
										<option value="BS Information System"{{ old('course') == 'BS Information System' ? ' selected':'' }}>BS Information System</option>
										<option value="BS Information Technology"{{ old('course') == 'BS Information Technology' ? ' selected':'' }}>BS Information Technology</option>
									</select>
									<label for="course">Program Course and Major</label>
								</div>

								<div class="row">
									<div class="col-sm-6">
										<div class="mb-3 form-floating">
											<input type="text" name="office_assigned" value="{{ old('office_assigned') }}" class="form-control" id="office_assigned" placeholder="Office assigned" oninput="this.className='form-control'" required>
											<label for="office_assigned">Office Assigned</label>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="mb-3 form-floating">
											<input type="text" name="office_location" value="{{ old('office_location') }}" class="form-control" id="office_location" placeholder="Office location" oninput="this.className='form-control'" required>
											<label for="office_location">Office Location</label>
										</div>
									</div>
								</div>

							</div>

							<div class="tab">	

								<div class="mb-3 form-floating">
									<input type="text" name="city" value="{{ old('city') }}" class="form-control" id="city" placeholder="City Address" oninput="this.className='form-control'" required>
									<label for="city">City Address</label>
								</div>
							
								<div class="mb-3 form-floating">
									<input type="text" name="province" value="{{ old('province') }}" class="form-control" id="province" placeholder="Province Address" oninput="this.className='form-control'" required>
									<label for="province">Province Address</label>
								</div>
						
								
								<div class="row">
									<div class="col-sm-6">
										<div class="mb-3 form-floating">
											<input type="text" name="guardian_name" value="{{ old('guardian_name') }}" class="form-control" id="guardian_name" placeholder="Name of Guardian" oninput="this.className='form-control'" required>
											<label for="guardian_name">Name of Guardian</label>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="mb-3 form-floating">
											<input type="text" name="guardian_phone" value="{{ old('guardian_phone') }}" class="form-control" id="guardian_phone" placeholder="Guardian Phone Number" oninput="this.className='form-control'" required>
											<label for="guardian_phone">Guardian Phone Number</label>
										</div>	
									</div>
								</div>

								<div class="mb-3 form-floating">
									<select name="required_hours" class="form-control" id="required_hours" placeholder="Total number of hours" oninput="this.className='form-control'" required>
										<option value="150"{{ old('required_hours') == '150' ? ' selected':'' }}>150 hours</option>
										<option value="200"{{ old('required_hours') == '200' ? ' selected':'' }}>200 hours</option>
										<option value="300"{{ old('required_hours') == '300' ? ' selected':'' }}>300 hours</option>
										<option value="600"{{ old('required_hours') == '600' ? ' selected':'' }}>600 hours</option>
									</select>
									<label for="required_hours">Total Number of hours</label>
								</div>
							</div>
							
							<div class="tab">
								<div class="card">
									<div class="card-body" style="padding:0;" align="center">
										<div class="camera">
											<video id="video">Video stream not available.</video>
										</div>
										<canvas id="canvas"></canvas>
										<div class="output">
											<img id="photo" alt="The screen capture will appear in this box.">
										</div>
										<input type="hidden" name="photo" value="">
									</div>
									<div class="card-footer">
										<div class="row justify-content-center">
											<div class="col-sm-6">
												<button type="button" onclick="jQuery('.camera').fadeIn();jQuery('.output').hide();" class="btn btn-secondary w-100">Show Camera</button>
											</div>
											<div class="col-sm-6">
												<button id="startbutton" class="btn btn-secondary w-100">Take a photo</button>
											</div>
										</div>
								    </div>
								</div>
	            			</div>

	            			<div class="row mt-3">
	            				<div class="col-sm-6">
	            					<button type="button" class="btn btn-secondary w-100 rounded-pill" id="prevBtn" onclick="nextPrev(-1)">Previous</button>	
	            				</div>
	            				<div class="col-sm-6">
	            					<button type="button" class="btn btn-success w-100 rounded-pill" id="nextBtn" onclick="nextPrev(1)">Next</button>
	            				</div>
	            			</div>

	            			<!-- Circles which indicates the steps of the form: -->
	            			<div style="text-align:center;margin-top:40px;">
	            				<span class="step"></span>
	            				<span class="step"></span>
	            				<span class="step"></span>
	            				<span class="step"></span>
	            				<span class="step"></span>
	            			</div>
	            
				        </form>

					</div>
				</div>
				
			</div>
		</div>
	</div>	
</div>

<script>
var $=jQuery;

var loaded = false;
var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
  // This function will display the specified tab of the form ...
  var x = document.getElementsByClassName("tab");

  x[n].style.display = "block";
  
  // ... and fix the Previous/Next buttons:
  if (n == 0) {
  	document.getElementById("prevBtn").style.display = "none";
  } else {
  	document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {

  	if ($('input[name="photo"]').val()=='') {
  		$('#nextBtn').prop('disabled',true);
  	} else {
  		$('#nextBtn').prop('disabled',false);
  	}
  	
  	document.getElementById("nextBtn").innerHTML = "Submit";

  } else {
  	
  	$('#nextBtn').prop('disabled',false);
  	document.getElementById("nextBtn").innerHTML = "Next";
  }
  // ... and run a function that displays the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form... :
  if (currentTab >= x.length) {
    //...the form gets submitted:
    document.getElementById("regForm").submit();
    return false;
}
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");
  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false:
      valid = false;
  }
}
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
  	document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
  	x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class to the current step:
  x[n].className += " active";
}
// This method will trigger user permissions
function onScanSuccess(decodedText, decodedResult) {
    // Handle on success condition with the decoded text or result.
    console.log(`Scan result: ${decodedText}`);
    sendData(decodedText);
}

function openScanner(){
	var html5QrcodeScanner = new Html5QrcodeScanner("reader", { fps: 10, qrbox: 250 });	
	html5QrcodeScanner.render(onScanSuccess);
	$('.btnscanner').hide();
}

function sendData(qr) {
    //other js which show block 
    jQuery('#loader').show();
    if(loaded) return;

    //ajax which load content to block
    $.ajax({
        type: "GET",
        url: 'log/'+qr,
        dataType: "json",
        data: {code:qr},
        success: function(data) {
            console.log(data);
            var toastLiveExample = document.getElementById('liveToast');
            var toast = new bootstrap.Toast(toastLiveExample);
            if (data.error) {
            	$('.toast').addClass('bg-'+data.error_type);
            	$('.toast').removeClass('bg-success');
            } else {
            	$('.toast').addClass('bg-success');
            	$('.toast').removeClass('bg-danger');
            	$('.toast').removeClass('bg-warning');
            }

            $('.toast-body').html(data.message);
    		toast.show();
        },
        complete: function(){
            jQuery('#loader').hide();
            loaded = false;
        },
    });

    loaded = true;
}

</script>
<script src="{{ asset('scripts.js') }}"></script>
@include('inc.footer')