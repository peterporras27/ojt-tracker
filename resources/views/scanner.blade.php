<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('styles.css') }}">
	<title>ISAT OJT Tracking System</title>
</head>
<body>
	<div class="container mt-5">
		<div class="row justify-content-center">
			<div class="col-sm-6">
				<div style="width: 500px" id="reader"></div>
			</div>
		</div>
	</div>

	<script src="{{ asset('jquery-3.6.0.min.js') }}"></script>
	<script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('html5-qrcode.min.js') }}"></script>
	<script>
		// This method will trigger user permissions
		function onScanSuccess(decodedText, decodedResult) {
		    // Handle on success condition with the decoded text or result.
		    console.log(`Scan result: ${decodedText}`, decodedResult);
		    html5QrcodeScanner.clear();
		    html5QrCode.stop().then((ignore) => {
			  // QR Code scanning is stopped.
			  console.log('stopped');
			}).catch((err) => {
			  // Stop failed, handle it.
			});
		}

		var html5QrcodeScanner = new Html5QrcodeScanner(
			"reader", { fps: 10, qrbox: 250 });
		html5QrcodeScanner.render(onScanSuccess);
	</script>
</body>
</html>