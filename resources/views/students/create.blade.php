@include('inc.header')
@include('inc.navbar')
<div class="nav-scroller bg-body shadow-sm">
  <nav class="nav nav-underline container" aria-label="Secondary navigation">
    <a class="nav-link badge rounded-pill bg-secondary mt-2 mb-2" href="{{ route('dashboard') }}">Go Back</a>
  </nav>
</div>

<div class="container mt-5">
	@include('inc.errors')

	<form action="{{ route('student.store') }}" method="POST">
		@csrf
		<div class="row">
			<div class="col-sm-4">
				<div class="card">
					<h5 class="card-header">Student Info</h5>
					<div class="card-body">
						
						<div class="mb-3">
							<label for="first_name" class="form-label">First Name</label>
							<input type="text" name="first_name" value="{{ old('first_name') }}" class="form-control" id="first_name" placeholder="">
						</div>
						<div class="mb-3">
							<label for="last_name" class="form-label">Last Name</label>
							<input type="text" name="last_name" value="{{ old('last_name') }}" class="form-control" id="last_name" placeholder="">
						</div>
						<div class="mb-3">
							<label for="middle_name" class="form-label">Middle Name</label>
							<input type="text" name="middle_name" value="{{ old('middle_name') }}" class="form-control" id="middle_name" placeholder="">
						</div>
						<div class="row">
							<div class="col-sm-6">
								<div class="mb-3">
									<label for="birthday" class="form-label">Birthday</label>
									<input type="date" name="birthday" value="{{ old('birthday') }}" class="form-control" id="birthday" placeholder="">
								</div>
							</div>
							<div class="col-sm-6">
								<div class="mb-3">
									<label for="gender" class="form-label">Gender</label>
									<select name="gender" name="gender" class="form-control" id="gender">
										<option value="Male"{{ old('gender') == 'Male' ? ' selected':'' }}>Male</option>
										<option value="Female"{{ old('gender') == 'Female' ? ' selected':'' }}>Female</option>
									</select>
								</div>
							</div>
						</div>
						<div class="mb-3">
							<label for="phone" class="form-label">Phone Number</label>
							<input type="text" name="phone" value="{{ old('phone') }}" class="form-control" id="phone" placeholder="">
						</div>
						<div class="mb-3">
							<label for="guardian_name" class="form-label">Name of Guardian</label>
							<input type="text" name="guardian_name" value="{{ old('guardian_name') }}" class="form-control" id="guardian_name" placeholder="">
						</div>
						<div class="mb-3">
							<label for="guardian_phone" class="form-label">Guardian Phone Number</label>
							<input type="text" name="guardian_phone" value="{{ old('guardian_phone') }}" class="form-control" id="guardian_phone" placeholder="">
						</div>

					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="card">
					<h5 class="card-header">Student Info</h5>
					<div class="card-body">

						<div class="mb-3">
							<label for="student_number" class="form-label">Student Number</label>
							<input type="text" name="student_number" value="{{ old('student_number') }}" class="form-control" id="student_number" placeholder="">
						</div>
						<div class="mb-3">
							<label for="course" class="form-label">Program Course and Major</label>
							<select  name="course" value="{{ old('course') }}" class="form-control" id="course">
								<option value="">Select</option>
								<option value="BS Computer Science"{{ old('course') == 'BS Computer Science' ? ' selected':'' }}>BS Computer Science</option>
								<option value="BS Information System"{{ old('course') == 'BS Information System' ? ' selected':'' }}>BS Information System</option>
								<option value="BS Information Technology"{{ old('course') == 'BS Information Technology' ? ' selected':'' }}>BS Information Technology</option>
							</select>
						</div>
						<div class="mb-3">
							<label for="year_section" class="form-label">Year and Section</label>
							<select name="year_section" class="form-control" id="year_section">
								<option value="">Select</option>
								<option value="4-A"{{ old('year_section') == '4-A' ? ' selected':''}}>4-A</option>
								<option value="4-B"{{ old('year_section') == '4-B' ? ' selected':''}}>4-B</option>
								<option value="4-C"{{ old('year_section') == '4-C' ? ' selected':''}}>4-C</option>
								<option value="4-D"{{ old('year_section') == '4-D' ? ' selected':''}}>4-D</option>
								<option value="4-E"{{ old('year_section') == '4-E' ? ' selected':''}}>4-E</option>
							</select>
						</div>
						<div class="mb-3">
							<label for="office_assigned" class="form-label">Office Assigned</label>
							<input type="text" name="office_assigned" value="{{ old('office_assigned') }}" class="form-control" id="office_assigned" placeholder="">
						</div>
						<div class="mb-3">
							<label for="office_location" class="form-label">Office Location</label>
							<input type="text" name="office_location" value="{{ old('office_location') }}" class="form-control" id="office_location" placeholder="">
						</div>
						<div class="mb-3">
							<label for="city" class="form-label">City Address</label>
							<input type="text" name="city" value="{{ old('city') }}" class="form-control" id="city" placeholder="">
						</div>
						<div class="mb-3">
							<label for="province" class="form-label">Province Address</label>
							<input type="text" name="province" value="{{ old('province') }}" class="form-control" id="province" placeholder="">
						</div>

					</div>
				</div>
			</div>

			<div class="col-sm-4">
				<div class="card">
					<h5 class="card-header">Profile Picture</h5>
					<div class="card-body" align="center">
						<div class="camera">
							<video id="video">Video stream not available.</video>
						</div>
						<canvas id="canvas"></canvas>
						<div class="output">
							<img id="photo" src="" alt="Click the show camera button to begin.">
						</div>
					</div>
					<div class="card-footer">
						<div class="row justify-content-center">
							<div class="col-sm-6 mt-2">
								<button type="button" onclick="jQuery('.camera').fadeIn();jQuery('.output').hide();" class="btn btn-secondary w-100">Show Camera</button>
							</div>
							<div class="col-sm-6 mt-2">
								<button id="startbutton" class="btn btn-secondary w-100">Take a photo</button>
							</div>
						</div>
				    </div>
				</div>
				<div class="card mt-3">
					<h5 class="card-header">Login Details</h5>
					<div class="card-body">
						<div class="mb-3">
							<label for="email" class="form-label">Email</label>
							<input type="email" name="email" value="{{ old('email') }}" class="form-control" id="email" placeholder="">
						</div>
						<div class="row">
							<div class="col-sm-6">
								<div class="mb-3">
									<label for="password" class="form-label">Password</label>
									<input type="password" name="password" class="form-control" id="password" placeholder="">
								</div>
							</div>
							<div class="col-sm-6">
								<div class="mb-3">
									<label for="password_confirmation" class="form-label">Confirm Password</label>
									<input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="">
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>

		<input type="hidden" name="photo" value="">
		<button type="submit" class="btn btn-primary mt-3 mb-5">Save Changes</button>
	</form>


</div>

@include('inc.footer')