@include('inc.header')
@include('inc.navbar')
<div class="nav-scroller bg-body shadow-sm">
	<nav class="nav nav-underline container" aria-label="Secondary navigation">
		<a class="nav-link badge rounded-pill bg-success mt-2 mb-2" href="{{ route('student.create') }}">Register New Student +</a>
	</nav>
</div>

<div class="container mt-5">
	<h3>Students</h3>
	<div class="row">
		<div class="card">
			<div class="card-body">
				<form>
					<div class="row">
						<div class="col-sm-3 mt-2">
							<input type="text" value="{{ $params['name'] }}" class="form-control form-control-sm" name="name" placeholder="Name">
						</div>
						<div class="col-sm-2 mt-2">
							<select name="year_section" class="form-control form-control-sm" id="year_section">
								<option value="">Year & Section</option>
								<option value="4-A"{{ $params['year_section'] == '4-A' ? ' selected':''}}>4-A</option>
								<option value="4-B"{{ $params['year_section'] == '4-B' ? ' selected':''}}>4-B</option>
								<option value="4-C"{{ $params['year_section'] == '4-C' ? ' selected':''}}>4-C</option>
								<option value="4-D"{{ $params['year_section'] == '4-D' ? ' selected':''}}>4-D</option>
								<option value="4-E"{{ $params['year_section'] == '4-E' ? ' selected':''}}>4-E</option>
							</select>
						</div>
						<div class="col-sm-2 mt-2">
							<input type="text" value="{{ $params['student_number'] }}" class="form-control form-control-sm" name="student_number" placeholder="Student Number">
						</div>
						<div class="col-sm-2 mt-2">
							<input type="text" value="{{ $params['office_assigned'] }}" class="form-control form-control-sm" name="office_assigned" placeholder="Office Assigned">
						</div>
						<div class="col-sm-2 mt-2">
							<input type="text" value="{{ $params['office_location'] }}" class="form-control form-control-sm" name="office_location" placeholder="Office Location">
						</div>
						<div class="col-sm-1 mt-2">
							<button class="btn btn-success btn-sm" type="submit">Search</button>
						</div>
					</div>
				</form>
				@if($students->count())
				<hr>
				<div class="table-responsive">
					<table class="table table-hover table-striped table-bordered table-sm mt-3">
						<thead>
							<tr>
								<th>Name</th>
								<th>Year & Section</th>
								<th>Course</th>
								<th>Student Number</th>
								<th>Office Assigned</th>
								<th>Office Location</th>
								<th>Options</th>
							</tr>
						</thead>
						<tbody>
							@foreach($students as $student)
							<tr>
								<td>
									@if($student->photo)
									<img style="height:50px;" src="{{ asset('storage/'.$student->photo) }}" class="img-fluid rounded-start" alt="...">
									@endif
									{{ $student->first_name.' '. $student->middle_name .' '.$student->last_name }}
								</td>
								<td>{{ $student->year_section }}</td>
								<td>{{ $student->course }}</td>
								<td>{{ $student->student_number }}</td>
								<td>{{ $student->office_assigned }}</td>
								<td>{{ $student->office_location }}</td>
								<td>
									<a class="badge bg-secondary" href="{{ route('student.show',$student->id) }}">Info</a> |
									<a class="badge bg-success" href="{{ route('student.edit',$student->id) }}">Edit</a> |
									<a class="badge bg-danger" href="#" data-bs-toggle="modal" data-bs-target="#student-{{ $student->id }}">Delete</a>
									<!-- Modal -->
									<div class="modal fade" id="student-{{ $student->id }}" tabindex="-1" aria-labelledby="student-label-{{ $student->id }}" aria-hidden="true">
										<div class="modal-dialog">
											<form action="{{ route('student.destroy',$student->id) }}" method="POST">
											@csrf
											@method('DELETE')
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title" id="student-label-{{ $student->id }}">Delete Student</h5>
													<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
												</div>
												<div class="modal-body">
													<strong>Are you sure you</strong> wish to delete <strong>{{ $student->first_name.' '.$student->last_name }}</strong> and all records associated with this user?
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
													<button type="submit" class="btn btn-danger">Delete</button>
												</div>
												</form>
											</div>
										</div>
									</div>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>

				{{ $students->links() }}

				@else
					@if($search)
						<div class="alert alert-primary d-flex align-items-center mt-3" role="alert">
							<svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Info:"><use xlink:href="#info-fill"/></svg>
							<div>No search results found in database.</div>
						</div>
					@else
						<div class="alert alert-primary d-flex align-items-center mt-3" role="alert">
							<svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Info:"><use xlink:href="#info-fill"/></svg>
							<div>There are no data's to display at the moment.</div>
						</div>
					@endif
				@endif

			</div>
		</div>
	</div>	
</div>

@include('inc.footer')