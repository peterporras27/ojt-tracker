@include('inc.header')
@include('inc.navbar')
<div class="container mt-5">
	
	<div class="row">
		<div class="col">
			<div class="card">
				<div class="card-header">
				    Details
				</div>
				<div class="card-body">
					@include('inc.errors')
					<ul class="nav nav-tabs mt-1" id="myTab" role="tablist">
						<li class="nav-item" role="presentation">
							<button class="nav-link{{ $tabs['home'] ? ' active':'' }}" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">Profile</button>
						</li>
						<li class="nav-item" role="presentation">
							<button class="nav-link{{ $tabs['profile'] ? ' active':'' }}" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Daily Time Record</button>
						</li>
						<li class="nav-item" role="presentation">
							<button class="nav-link{{ $tabs['portfolio'] ? ' active':'' }}" id="portfolio-tab" data-bs-toggle="tab" data-bs-target="#portfolio" type="button" role="tab" aria-controls="portfolio" aria-selected="false">Portfolio</button>
						</li>
					</ul>
					<div class="tab-content" id="myTabContent">
						<div class="tab-pane fade{{ $tabs['home'] ? ' show active':'' }}" id="home" role="tabpanel" aria-labelledby="home-tab">
							<div class="tab-pane-wrapper">
								<div class="row">
									<div class="col-sm-3" align="center">
										<div class="card">
											<img src="data:image/png;base64,{!! base64_encode(QrCode::format('png')->size(600)->generate($student->student_number)) !!}" class="img-fluid" alt="...">
											<div class="card-body">
												<h5 class="card-title">QR CODE</h5>
												<p class="card-text">Download and save a copy of this QR code always to be used when loging in and out of the office.</p>
												<a href="{{ route('download',$student->id) }}" class="btn btn-primary">Download QR</a>
											</div>
										</div>
									</div>
									<div class="col-sm-9">
										<div class="card mb-3">
											<div class="row g-0">
												<div class="col-md-4">
													<img style="width:320px;" src="{{ asset('storage/'.$student->photo) }}" class="img-fluid rounded-start" alt="...">
												</div>
												<div class="col-md-8">
													<div class="card-body">
														<div class="table-responsive">
															<table class="table table-striped table-bordered table-sm mb-0">
																<tr>
																	<td><b>Full Name:</b></td>
																	<td>{{$student->first_name.' '.$student->middle_name.' '.$student->last_name}}</td>
																</tr>
																<tr>
														        	<td><b>Course:</b></td>
														        	<td>{{$student->course}}</td>
														        </tr>
														        <tr>
														        	<td><b>Year and Section:</b></td>
														        	<td>{{$student->year_section}}</td>
														        </tr>
														        <tr>
														        	<td><b>Office Assigned</b></td>
														        	<td>{{ $student->office_assigned }}</td>
														        </tr>
														        <tr>
														        	<td><b>Office Location</b></td>
														        	<td>{{ $student->office_location }}</td>
														        </tr>
														        <tr>
														        	<td><b>Phone:</b></td>
														        	<td>{{$student->phone}}</td>
														        </tr>
														        <tr>
														        	<td><b>Email:</b></td>
														        	<td>{{$student->email}}</td>
														        </tr>
															</table>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="table-responsive">
											<table class="table table-striped table-bordered table-sm">
												<tr>
										        	<td><b>Gender:</b></td>
										        	<td>{{$student->gender}}</td>
										        </tr>
										        <tr>
										        	<td><b>Birthday:</b></td>
										        	<td>{{$student->birthday}}</td>
										        </tr>
										        <tr>
										        	<td><b>City Address:</b></td>
										        	<td>{{$student->city}}</td>
										        </tr>
										        <tr>
										        	<td><b>Province Address:</b></td>
										        	<td>{{$student->province}}</td>
										        </tr>
										        <tr>
										        	<td><b>Student Number:</b></td>
										        	<td>{{$student->student_number}}</td>
										        </tr>
										        <tr>
										        	<td><b>Guardian Name:</b></td>
										        	<td>{{$student->guardian_name}}</td>
										        </tr>
										        <tr>
										        	<td><b>Guardian Phone:</b></td>
										        	<td>{{$student->guardian_phone}}</td>
										        </tr>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane fade{{ $tabs['profile'] ? ' show active':'' }}" id="profile" role="tabpanel" aria-labelledby="profile-tab">
							<div class="tab-pane-wrapper">
								<form method="GET">
									<input type="hidden" name="tab" value="profile">
									<div class="row">
										<div class="col-sm-4 mt-2">
											<input type="date" name="from" value="{{ request()->get('from') }}" class="form-control form-control-sm" placeholder="From">
										</div>
										<div class="col-sm-4 mt-2">
											<input type="date" name="to" value="{{ request()->get('to') }}" class="form-control form-control-sm" placeholder="From">
										</div>
										<div class="col-sm-4 mt-2">
											<button type="submit" class="btn btn-primary btn-sm">Filter</button>
											<a target="_blank" href="{{ route('student.print',['id'=>$student->id,'from'=>request()->get('from'),'to'=>request()->get('to')]) }}" class="btn btn-success btn-sm">Print</a>
										</div>
									</div>
								</form>
								<hr>
								@if( count($days) )
								<div class="table-responsive">
									<table id="timesheet" class="table table-sm table-striped table-bordered">
									  <thead>
									  	<tr>
									  		<th>Date</th>
									  		<th></th>
									  		<th>Time in</th>
									  		<th>Time out</th>
									  		<th>No. of Hours</th>
									  	</tr>
									  </thead>
									  <tbody>
									  	@foreach( $days as $date => $day )
									  	<tr>
									  		<td>
									  			<div class="row">
									  				<div class="col text-center">
									  					{{ $date }}
									  				</div>
									  			</div>
									  		</td>
									  		<td>
									  			AM<div class="sep mt-1 mb-1"></div>PM
									  		</td>
									  		<td>
									  			<?php 
									  			$is_in_am = false;
									  			$is_in_pm = false; 
									  			$am_time_in = false;
									  			$pm_time_in = false;
									  			$am_time_out = false;
									  			$pm_time_out = false;
									  			$am_status = 'apporved';
									  			$pm_status = 'apporved';
									  			?>
									  			@if(isset($day['AM']['time_in']))
									  				<?php 
									  				$am_in = $day['AM']['time_in'];
									  				$am_time_in = $am_in->created_at->format('h:i');
									  				$is_in_am = true;
									  				?>
									  				{{ $am_time_in }}
									  			@else
									  				&nbsp;
									  			@endif
									  			<div class="sep mt-1 mb-1"></div>
									  			@if(isset($day['PM']['time_in']))
									  				<?php 
									  				$pm_in = $day['PM']['time_in'];
									  				$pm_time_in = $pm_in->created_at->format('h:i');
									  				$is_in_pm = true; 
									  				?>
									  				{{ $pm_time_in }}
									  			@else 
									  				&nbsp;
									  			@endif
									  		</td>
									  		<td>
									  			@if(isset($day['AM']['time_out']))
									  				<?php 
									  				$am_out = $day['AM']['time_out']; 
									  				$am_time_out = $am_out->created_at->format('h:i');
									  				$am_status = $am_out->status;
									  				?>
									  				{{ $am_time_out }}
									  				@if($am_status=='pending')
									  					&nbsp;<small class="text-danger"><i>(Pending Approval)</i></small>
									  				@endif
									  			@else 
									  				@if( $is_in_am && date('m-d-Y') != $date && auth()->user()->role == 'student' )
									  					<?php 
									  					$am_in = $day['AM']['time_in'];
								  						$s = (int) $am_in->created_at->format('h') + 1;
									  					?>
									  					<form action="{{ route('student.log',$student->id) }}" method="POST">
									  						@csrf
									  						<input type="hidden" name="date" value="{{ $date }}">
									  						<input type="hidden" name="am_pm" value="AM">
										  					<div class="change-time input-group input-group-sm mt-1 float-end">
										  						<span class="input-group-text">AM</span>
																<select class="form-select">
																	@for($i=$s; $i <= 11; $i++)
																		<option value="{{ $i < 10 ? '0'.$i:$i }}">{{ $i < 10 ? '0'.$i:$i }}</option>
																	@endfor
																</select>
	  															<span class="input-group-text">:</span>
																<select class="form-select">
																	@for($i=0; $i <= 59; $i++)
																		<option value="{{ $i < 10 ? '0'.$i:$i }}">{{ $i < 10 ? '0'.$i:$i }}</option>
																	@endfor
																</select>
																<button class="btn btn-secondary" type="submit">
																	Update Timeout
																</button>
															</div>
														</form>
									  				@else
									  					&nbsp;
									  				@endif
									  			@endif
									  			<div class="sep mt-1 mb-1"></div>
									  			@if(isset($day['PM']['time_out']))
									  				<?php 
									  				$pm_out = $day['PM']['time_out'];
									  				$pm_time_out = $pm_out->created_at->format('h:i');
									  				$pm_status = $pm_out->status;
									  				?>
									  				<form action="{{ route('approve.log',$pm_out->id) }}" method="GET">
									  				{{ $pm_time_out }}
									  				@if($pm_status=='pending')
									  					@if(auth()->user()->role=='admin')
									  						&nbsp;<a href="#"
									  							onclick="event.preventDefault();this.closest('form').submit();" 
									  							class="badge bg-secondary badge-pill">&#10003; Approve timeout request</a>
									  					@else
									  						&nbsp;<small class="text-danger"><i>(Pending Approval)</i></small>
									  					@endif
									  				@endif
									  				</form>
									  			@else 
									  				@if( $is_in_pm && date('m-d-Y') != $date && auth()->user()->role == 'student' )
									  					<?php 
									  					$pm_in = $day['PM']['time_in'];
								  						$s = (int) $pm_in->created_at->format('h') + 1;
									  					?>
									  					<form action="{{ route('student.log',$student->id) }}" method="POST">
									  						@csrf
									  						<input type="hidden" name="date" value="{{ $date }}">
									  						<input type="hidden" name="am_pm" value="PM">
															<div class="change-time input-group input-group-sm mt-1 float-end">
																<span class="input-group-text">PM</span>
																<select name="hour" class="form-select">
																	@for($i=$s; $i <= 11; $i++)
																		<option value="{{ $i < 10 ? '0'.$i:$i }}">{{ $i < 10 ? '0'.$i:$i }}</option>
																	@endfor
																</select>
	  															<span class="input-group-text">:</span>
																<select name="minute" class="form-select">
																	@for($i=0; $i <= 59; $i++)
																		<option value="{{ $i < 10 ? '0'.$i:$i }}">{{ $i < 10 ? '0'.$i:$i }}</option>
																	@endfor
																</select>
																<button class="btn btn-secondary" type="submit">
																	Submit timeout
																</button>
															</div>
														</form>
									  				@else
									  					&nbsp;
									  				@endif
									  			@endif
									  		</td>
									  		<td>
									  			<?php 

									  			if ($am_time_in && $am_time_out && $am_status == 'approved' ) {
										  			$to_am_time = strtotime("0000-00-00 ".$am_time_in.":00");
													$from_am_time = strtotime("0000-00-00 ".$am_time_out.":00");
													$total_am = round(abs($to_am_time - $from_am_time) / 60,2);
													$convam = convertToHoursMins($total_am); 
													echo '<b>'.$convam['hours'].'</b> hrs <b>'.$convam['minutes'].'</b> min';

													$time['hours'] = $time['hours']+$convam['hours'];
													$time['minutes'] = $time['minutes']+$convam['minutes'];

												} else {

													echo "&nbsp;";
												}

												?>
									  			<div class="sep mt-1 mb-1"></div>
									  			<?php 

									  			if ($pm_time_in && $pm_time_out && $pm_status == 'approved' ) {
										  			$to_pm_time = strtotime("0000-00-00 ".$pm_time_in.":00");
													$from_pm_time = strtotime("0000-00-00 ".$pm_time_out.":00");
													$total_pm = round(abs($to_pm_time - $from_pm_time) / 60,2);
													$convpm = convertToHoursMins($total_pm); 
													echo '<b>'.$convpm['hours'].'</b> hrs <b>'.$convpm['minutes'].'</b> min';

													$time['hours'] = $time['hours']+$convpm['hours'];
													$time['minutes'] = $time['minutes']+$convpm['minutes'];

												} else {

													echo "&nbsp;";
												}

									  			?>
									  		</td>
									  	</tr>
									  	@endforeach
										<?php $totalHours = (intdiv($time['minutes'], 60) + $time['hours']); ?>
										<?php $remainingMinutes = ($time['minutes'] % 60); ?>
									  	<tr class="table-success">
									  		<td>Rendered Hours:</td>
									  		<td></td>
									  		<td></td>
									  		<td></td>
									  		<td>
									  			<b>{{ $totalHours }}</b> hrs
												<b>{{ $remainingMinutes }}</b> min
									  		</td>
									  	</tr>
									  	<tr class="table-warning">
									  		<td>Hours Required:</td>
									  		<td></td>
									  		<td></td>
									  		<td></td>
									  		<td><b>{{ $student->required_hours }}</b> hrs</td>
									  	</tr>
									  	<tr class="table-dark">
									  		<td>Hours Remaining:</td>
									  		<td></td>
									  		<td></td>
									  		<td></td>
									  		<td>
									  			<?php $remaining_hrs = ($student->required_hours - $totalHours); ?>
									  			<?php $remaining_min = ($remainingMinutes > 0) ? (60-$remainingMinutes):0; ?>
												<?php $remaining_hrs = ($remaining_min > 0) ? ($remaining_hrs-1):$remaining_hrs; ?>
												
												<b>{{ $remaining_hrs }}</b> hrs
												<b>{{ $remaining_min }}</b> min
											</td>
									  	</tr>
									  </tbody>
									</table>
								</div>
								@else

								<div class="alert alert-primary d-flex align-items-center mt-3" role="alert">
									<svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Info:"><use xlink:href="#info-fill"/></svg>
									<div>There are no data's to display at the moment.</div>
								</div>

								@endif
							
							</div>
						</div>
						<div class="tab-pane fade{{ $tabs['portfolio'] ? ' show active':'' }}" id="portfolio" role="tabpanel" aria-labelledby="portfolio-tab">
							<div class="tab-pane-wrapper">
								
								@if(auth()->user()->role=='student')
								<div class="row">
									<div class="col-sm-4">
										<div class="input-group">
											<input type="file" name="file" class="form-control" id="file-upload" aria-describedby="file-upload" aria-label="Upload">
											<button data-id="{{ $student->id }}" class="btn btn-outline-success" type="button" id="ajax-upload">Upload</button>
										</div>
									</div>
								</div>
								@endif
								
								<div class="table-responsive">
									<table id="file-list" class="table mt-2 table-sm table-bordered table-hover">
										<thead>
											<tr>
												<th>File Name</th>
												<th>File Type</th>
												<th>Upload Date</th>
												<th>Actions</th>
											</tr>
										</thead>
										<tbody>
											@if($files)
												@foreach($files as $file)
												<tr>
													<td>
														<a target="_blank" href="{{ asset('/storage/'.$file->hash_name) }}">
															{{ $file->name }}
														</a>
													</td>
													<td>{{ strtoupper( $file->extension ) }}</td>
													<td>{{ $file->created_at->format('F j, Y, g:i a') }}</td>
													<td><button onclick="deleteFile(this)" data-id="{{ $file->id }}" class="btn btn-danger btn-sm">Delete</button></td>
												</tr>
												@endforeach
											@endif
										</tbody>
									</table>
								</div>

							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>	
</div>
<?php 
function convertToHoursMins($time) {
    if ($time < 1) { return; }
    $hours = floor($time / 60);
    $minutes = ($time % 60);

    return [
    	'hours' => $hours, 
    	'minutes' => $minutes
    ];
}
?>
@include('inc.footer')