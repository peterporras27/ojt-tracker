@include('inc.header')
@include('inc.navbar')
<div class="nav-scroller bg-body shadow-sm">
  <nav class="nav nav-underline container" aria-label="Secondary navigation">
    <a class="nav-link badge rounded-pill bg-secondary mt-2 mb-2" href="{{ route('dashboard') }}">Go Back</a>
  </nav>
</div>

<div class="container mt-5">
	@include('inc.errors')

	<form action="{{ route('student.update',$student->id) }}" method="POST">
		@csrf
		@method('PUT')

		<div class="row">
			<div class="col-sm-4">
				<div class="card">
					<h5 class="card-header">Student Info</h5>
					<div class="card-body">
						
						<div class="mb-3">
							<label for="first_name" class="form-label">First Name</label>
							<input type="text" name="first_name" value="{{ $student->first_name }}" class="form-control" id="first_name" placeholder="">
						</div>
						<div class="mb-3">
							<label for="last_name" class="form-label">Last Name</label>
							<input type="text" name="last_name" value="{{ $student->last_name }}" class="form-control" id="last_name" placeholder="">
						</div>
						<div class="mb-3">
							<label for="middle_name" class="form-label">Middle Name</label>
							<input type="text" name="middle_name" value="{{ $student->middle_name }}" class="form-control" id="middle_name" placeholder="">
						</div>
						<div class="row">
							<div class="col-sm-6">
								<div class="mb-3">
									<label for="birthday" class="form-label">Birthday</label>
									<input type="date" name="birthday" value="{{ $student->birthday }}" class="form-control" id="birthday" placeholder="">
								</div>
							</div>
							<div class="col-sm-6">
								<div class="mb-3">
									<label for="gender" class="form-label">Gender</label>
									<select name="gender" name="gender" class="form-control" id="gender">
										<option value="Male"{{ $student->gender == 'Male' ? ' selected':'' }}>Male</option>
										<option value="Female"{{ $student->gender == 'Female' ? ' selected':'' }}>Female</option>
									</select>
								</div>
							</div>
						</div>
						<div class="mb-3">
							<label for="phone" class="form-label">Phone Number</label>
							<input type="text" name="phone" value="{{ $student->phone }}" class="form-control" id="phone" placeholder="">
						</div>
						<div class="mb-3">
							<label for="guardian_name" class="form-label">Name of Guardian</label>
							<input type="text" name="guardian_name" value="{{ $student->guardian_name }}" class="form-control" id="guardian_name" placeholder="">
						</div>
						<div class="mb-3">
							<label for="guardian_phone" class="form-label">Guardian Phone Number</label>
							<input type="text" name="guardian_phone" value="{{ $student->guardian_phone }}" class="form-control" id="guardian_phone" placeholder="">
						</div>

					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="card">
					<h5 class="card-header">Student Info</h5>
					<div class="card-body">

						<div class="mb-3">
							<label for="student_number" class="form-label">Student Number</label>
							<input type="text" name="student_number" value="{{ $student->student_number }}" class="form-control" id="student_number" placeholder="">
						</div>
						<div class="mb-3">
							<label for="course" class="form-label">Program Course and Major</label>
							<select  name="course" value="{{ $student->course }}" class="form-control" id="course">
								<option value="">Select</option>
								<option value="BS Computer Science"{{ $student->course == 'BS Computer Science' ? ' selected':'' }}>BS Computer Science</option>
								<option value="BS Information System"{{ $student->course == 'BS Information System' ? ' selected':'' }}>BS Information System</option>
								<option value="BS Information Technology"{{ $student->course == 'BS Information Technology' ? ' selected':'' }}>BS Information Technology</option>
							</select>
						</div>
						<div class="mb-3">
							<label for="year_section" class="form-label">Year and Section</label>
							<select name="year_section" class="form-control" id="year_section">
								<option value="">Select</option>
								<option value="4-A"{{ $student->year_section == '4-A' ? ' selected':''}}>4-A</option>
								<option value="4-B"{{ $student->year_section == '4-B' ? ' selected':''}}>4-B</option>
								<option value="4-C"{{ $student->year_section == '4-C' ? ' selected':''}}>4-C</option>
								<option value="4-D"{{ $student->year_section == '4-D' ? ' selected':''}}>4-D</option>
								<option value="4-E"{{ $student->year_section == '4-E' ? ' selected':''}}>4-E</option>
							</select>
						</div>
						<div class="mb-3">
							<label for="office_assigned" class="form-label">Office Assigned</label>
							<input type="text" name="office_assigned" value="{{ $student->office_assigned }}" class="form-control" id="office_assigned" placeholder="">
						</div>
						<div class="mb-3">
							<label for="office_location" class="form-label">Office Location</label>
							<input type="text" name="office_location" value="{{ $student->office_location }}" class="form-control" id="office_location" placeholder="">
						</div>
						<div class="mb-3">
							<label for="city" class="form-label">City Address</label>
							<input type="text" name="city" value="{{ $student->city }}" class="form-control" id="city" placeholder="">
						</div>
						<div class="mb-3">
							<label for="province" class="form-label">Province Address</label>
							<input type="text" name="province" value="{{ $student->province }}" class="form-control" id="province" placeholder="">
						</div>

					</div>
				</div>
			</div>

			<div class="col-sm-4">
				<div class="card">
					<h5 class="card-header">Profile Picture</h5>
					<div class="card-body" align="center">
						<div class="camera">
							<video id="video">Video stream not available.</video>
						</div>
						<canvas id="canvas"></canvas>
						<div class="output">
							<img id="photo" src="{{ asset('storage/'.$student->photo) }}" alt="The screen capture will appear in this box.">
						</div>
					</div>
					<div class="card-footer">
						<div class="row justify-content-center">
							<div class="col-sm-6">
								<button type="button" onclick="jQuery('.camera').fadeIn();jQuery('.output').hide();" class="btn btn-secondary w-100">Show Camera</button>
							</div>
							<div class="col-sm-6">
								<button id="startbutton" class="btn btn-secondary w-100">Take a photo</button>
							</div>
						</div>
				    </div>
				</div>
				<div class="card mt-3">
					<h5 class="card-header">Login Details</h5>
					<div class="card-body">
						<div class="mb-3">
							<label for="email" class="form-label">Email</label>
							<input type="email" name="email" value="{{ $student->email }}" class="form-control" id="email" placeholder="">
						</div>
						<div class="row">
							<div class="col-sm-6">
								<div class="mb-3">
									<label for="password" class="form-label">Password</label>
									<input type="password" name="password" class="form-control" id="password" placeholder="">
								</div>
							</div>
							<div class="col-sm-6">
								<div class="mb-3">
									<label for="password_confirmation" class="form-label">Confirm Password</label>
									<input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="">
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>

		<input type="hidden" name="photo" value="">
		<button type="submit" class="btn btn-primary mt-3 mb-5">Save Changes</button>
	</form>
</div>

@include('inc.footer')