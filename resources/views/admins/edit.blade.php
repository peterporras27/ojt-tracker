@include('inc.header')
@include('inc.navbar')
<div class="nav-scroller bg-body shadow-sm">
  <nav class="nav nav-underline container" aria-label="Secondary navigation">
    <a class="nav-link badge rounded-pill bg-secondary mt-2 mb-2" href="{{ route('admins') }}">Go Back</a>
  </nav>
</div>

<div class="container mt-5">
	@include('inc.errors')
	<form action="{{ route('admin.update',$user->id) }}" method="POST">
		@csrf
		@method('PUT')
		<div class="row">
			<div class="col-sm-6">
				<div class="card">
					<h5 class="card-header">Admin Info</h5>
					<div class="card-body">
						<div class="mb-3">
							<label for="first_name" class="form-label">First Name</label>
							<input type="text" name="first_name" value="{{ $user->first_name }}" class="form-control" id="first_name" placeholder="">
						</div>
						<div class="mb-3">
							<label for="last_name" class="form-label">Last Name</label>
							<input type="text" name="last_name" value="{{ $user->last_name }}" class="form-control" id="last_name" placeholder="">
						</div>
						<div class="mb-3">
							<label for="middle_name" class="form-label">Middle Name</label>
							<input type="text" name="middle_name" value="{{ $user->middle_name }}" class="form-control" id="middle_name" placeholder="">
						</div>
						<div class="mb-3">
							<label for="phone" class="form-label">Phone Number</label>
							<input type="text" name="phone" value="{{ $user->phone }}" class="form-control" id="phone" placeholder="">
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="card">
					<h5 class="card-header">Login Details</h5>
					<div class="card-body">
						<div class="mb-3">
							<label for="email" class="form-label">Email</label>
							<input type="email" name="email" value="{{ $user->email }}" class="form-control" id="email" placeholder="">
						</div>
						<div class="row">
							<div class="col-sm-6">
								<div class="mb-3">
									<label for="password" class="form-label">Password</label>
									<input type="password" name="password" class="form-control" id="password" placeholder="">
								</div>
							</div>
							<div class="col-sm-6">
								<div class="mb-3">
									<label for="password_confirmation" class="form-label">Confirm Password</label>
									<input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="">
								</div>
							</div>
						</div>
					</div>
				</div>

				<button type="submit" class="btn btn-primary mt-3 mb-5 float-end">Update Details</button>
			</div>
		</div>
	</form>


</div>

@include('inc.footer')