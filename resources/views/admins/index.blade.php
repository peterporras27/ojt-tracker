@include('inc.header')
@include('inc.navbar')
<div class="nav-scroller bg-body shadow-sm">
	<nav class="nav nav-underline container" aria-label="Secondary navigation">
		<a class="nav-link badge rounded-pill bg-success mt-2 mb-2" href="{{ route('admin.create') }}">Register New Admin +</a>
	</nav>
</div>

<div class="container mt-5">
	<h3>Administrators</h3>
	<div class="row">
		<div class="card">
			<div class="card-body">
				
				@if($admins->count())
				<div class="table-responsive">
					<table class="table table-hover table-striped table-bordered table-sm mt-3">
						<thead>
							<tr>
								<th>First Name</th>
								<th>Last Name</th>
								<th>Email</th>
								<th>Phone</th>
								<th>Options</th>
							</tr>
						</thead>
						<tbody>
							@foreach($admins as $admin)
							<tr>
								<td>{{ $admin->first_name }}</td>
								<td>{{ $admin->last_name }}</td>
								<td>{{ $admin->email }}</td>
								<td>{{ $admin->phone }}</td>
								<td>
									<a class="badge bg-success" href="{{ route('admin.edit',$admin->id) }}">Edit</a> |
									<a class="badge bg-danger" href="#" data-bs-toggle="modal" data-bs-target="#admin-{{ $admin->id }}">Delete</a>
									<!-- Modal -->
									<div class="modal fade" id="admin-{{ $admin->id }}" tabindex="-1" aria-labelledby="admin-label-{{ $admin->id }}" aria-hidden="true">
										<div class="modal-dialog">
											<form action="{{ route('admin.destroy',$admin->id) }}" method="POST">
											@csrf
											@method('DELETE')
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title" id="admin-label-{{ $admin->id }}">Delete Admin</h5>
													<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
												</div>
												<div class="modal-body">
													<strong>Are you sure you</strong> wish to delete <strong>{{ $admin->first_name.' '.$admin->last_name }}</strong> and all records associated with this user?
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
													<button type="submit" class="btn btn-danger">Delete</button>
												</div>
												</form>
											</div>
										</div>
									</div>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>

				{{ $admins->links() }}

				@else
					
					<div class="alert alert-primary d-flex align-items-center mt-3" role="alert">
						<svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Info:"><use xlink:href="#info-fill"/></svg>
						<div>There are no data's to display at the moment.</div>
					</div>
				
				@endif

			</div>
		</div>
	</div>	
</div>

@include('inc.footer')