<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<title>Print</title>
<style>
.small {font-size: 12px;}
.sep{border-bottom: 1px solid #ccc;}
#timesheet .text-center {padding-top: 10px;}
</style>
</head>
<body>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                <span>{{$student->first_name.' '.$student->middle_name.' '.$student->last_name}}</span>
                <span style="float:right;">
                    @if(!empty($title))
                        {{ $title }}
                    @endif
                </span>
                <table class="table-sm small">
                    <tr>
                        <td><b>Course</b>: {{$student->course}}</td>
                        <td><b>Year and Section</b>: {{$student->year_section}}</td>
                        <td><b>Office Assigned</b>: {{ $student->office_assigned }}</td>
                        <td><b>Office Location</b>: {{ $student->office_location }}</td>
                        <td><b>Phone</b>: {{$student->phone}}</td>
                    </tr>
                </table>
                @if( count($days) )
                    <table id="timesheet" class="table table-sm table-striped table-bordered small">
                        <thead>
                        <tr>
                            <th>Date</th>
                            <th>&nbsp;</th>
                            <th>Time in</th>
                            <th>Time out</th>
                            <th>No. of Hours</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach( $days as $date => $day )
                        <tr>
                            <td>
                                <div class="row">
                                    <div class="col text-center">
                                        {{ $date }}
                                    </div>
                                </div>
                            </td>
                            <td>
                                AM<div class="sep"></div>PM
                            </td>
                            <td>
                                @if(isset($day['AM']['time_in']))
                                    {{ $day['AM']['time_in'] }}
                                @else
                                    &nbsp;
                                @endif
                                <div class="sep"></div>
                                @if(isset($day['PM']['time_in']))
                                    {{ $day['PM']['time_in'] }}
                                @else 
                                    &nbsp;
                                @endif
                            </td>
                            <td>
                                @if(isset($day['AM']['time_out']))
                                    {{ $day['AM']['time_out'] }}
                                @else 
                                    &nbsp;
                                @endif
                                <div class="sep"></div>
                                @if(isset($day['PM']['time_out']))
                                    {{ $day['PM']['time_out'] }}
                                @else 
                                    &nbsp;
                                @endif
                            </td>
                            <td>
                                <?php 

                                if (isset($day['AM']['time_in']) && isset($day['AM']['time_out'])) {
                                    $to_am_time = strtotime("0000-00-00 ".$day['AM']['time_in'].":00");
                                    $from_am_time = strtotime("0000-00-00 ".$day['AM']['time_out'].":00");
                                    $total_am = round(abs($to_am_time - $from_am_time) / 60,2);
                                    $convam = convertToHoursMins($total_am); 
                                    echo '<b>'.$convam['hours'].'</b> hrs <b>'.$convam['minutes'].'</b> min';

                                    $time['hours'] = $time['hours']+$convam['hours'];
                                    $time['minutes'] = $time['minutes']+$convam['minutes'];

                                } else {

                                    echo "&nbsp;";
                                }

                                ?>
                                <div class="sep"></div>
                                <?php 

                                if (isset($day['PM']['time_in']) && isset($day['PM']['time_out'])) {
                                    $to_pm_time = strtotime("0000-00-00 ".$day['PM']['time_in'].":00");
                                    $from_pm_time = strtotime("0000-00-00 ".$day['PM']['time_out'].":00");
                                    $total_pm = round(abs($to_pm_time - $from_pm_time) / 60,2);
                                    $convpm = convertToHoursMins($total_pm); 
                                    echo '<b>'.$convpm['hours'].'</b> hrs <b>'.$convpm['minutes'].'</b> min';

                                    $time['hours'] = $time['hours']+$convpm['hours'];
                                    $time['minutes'] = $time['minutes']+$convpm['minutes'];

                                } else {

                                    echo "&nbsp;";
                                }

                                ?>
                            </td>
                        </tr>
                        @endforeach
                        <?php $totalHours = (intdiv($time['minutes'], 60) + $time['hours']); ?>
                        <?php $remainingMinutes = ($time['minutes'] % 60); ?>
                        <tr class="table-success">
                            <td>Rendered Hours:</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                <b>{{ $totalHours }}</b> hrs
                                <b>{{ $remainingMinutes }}</b> min
                            </td>
                        </tr>
                        <tr class="table-warning">
                            <td>Hours Required:</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><b>{{ $student->required_hours }}</b> hrs</td>
                        </tr>
                        <tr class="table-dark">
                            <td>Hours Remaining:</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                <?php $hrsrem = ($totalHours > 0) ? ($totalHours-1):0; ?>
                                <?php $minrem = ($remainingMinutes > 0) ? (60-$remainingMinutes):0; ?>
                                <b>{{ ($student->required_hours - $hrsrem) }}</b> hrs
                                <b>{{ $minrem }}</b> min
                            </td>
                        </tr>
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<script>window.print();</script>
</body>
</html>
<?php 
function convertToHoursMins($time) {
    if ($time < 1) { return; }
    $hours = floor($time / 60);
    $minutes = ($time % 60);

    return [
    	'hours' => $hours, 
    	'minutes' => $minutes
    ];
}
?>